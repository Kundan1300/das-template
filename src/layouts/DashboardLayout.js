import React from "react";
import styled from "styled-components";

import SideNav from "../containers/SideNav";
import Content from "../containers/Content";
import Flex from "../components/Flex";

const Wrap = styled(Flex)``;
const StyledContent = styled(Content)`
  background-color: #f3f3f3;
`;

const DashboardLayout = () => (
  <Wrap row>
    <SideNav />
    <StyledContent />
  </Wrap>
);

export default DashboardLayout;
