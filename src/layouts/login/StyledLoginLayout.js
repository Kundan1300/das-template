import styled from "styled-components";

import StyledButton from "../../components/Buttons";
import StyledPaper from "../../components/Cards/StyledPaper";
import StyledInput from "../../components/FormFields/StyledInput";
import Flex from "../../components/Flex";

export const Wrap = styled(Flex)``;
export const Left = styled(StyledPaper)`
  width: 36vw;
  display: flex;
  flex-direction: column;
  padding: 1.5rem 3rem;
  height: 100vh;
`;
export const Right = styled(Flex)`
  background-color: #7c93b1;
  width: calc(100vw - 36vw);
`;
export const Button = styled(StyledButton)`
  align-self: ${props => (props.full ? "unset" : "center")};
  padding: 0.5rem;
  width: ${props => (props.full ? "initial" : "fit-content")};
`;
export const Input = styled(StyledInput)``;
