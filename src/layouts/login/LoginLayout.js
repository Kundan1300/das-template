import React from "react";

import { Wrap, Left, Right, Input, Button } from "./StyledLoginLayout";
import { useInput } from "../../custom-hooks/useInput";
import StyledFormLabel from "../../components/FormFields/FormLabel";

const LoginLayout = ({ history, location }) => {
  const email = useInput("");
  const password = useInput("");

  const loginHandler = () => {
    const { from } = location.state || { from: "/" };

    localStorage.setItem("token", "cool");
    history.push(from);
  };

  return (
    <Wrap row>
      <Left>
        <h1 style={{ marginTop: "8rem" }}>Signin</h1>
        <StyledFormLabel mt={4} label="Email" />
        <Input
          mt={0.5}
          size="small"
          placeholder="example@example.com"
          type="email"
          {...email}
        />
        <StyledFormLabel mt={1.75} label="Password" />
        <Input
          mt={0.5}
          size="small"
          type="password"
          placeholder="xxxxxxx"
          {...password}
        />
        <Button full mt={2.75} size="small" callBack={() => loginHandler()}>
          Login
        </Button>
        <StyledFormLabel
          style={{ textAlign: "center" }}
          label="If you don't have an account"
          mt={4}
        />
        <Button
          mt={1}
          variant="text"
          size="small"
          callBack={() => loginHandler()}
        >
          Signup
        </Button>
      </Left>
      <Right></Right>
    </Wrap>
  );
};

export default LoginLayout;
