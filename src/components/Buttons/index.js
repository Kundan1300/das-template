import React from "react";
import Button from "@material-ui/core/Button";
import PropTypes from "prop-types";
import styled from "styled-components";

const Btn = styled(Button)`
  border-radius: 4px;
  letter-spacing: 1.25px;
  margin-top: ${props => (props.mt ? props.mt + "rem" : "0")};
  margin-bottom: ${props => (props.mb ? props.mb + "rem" : "0")};
  margin-left: ${props => (props.ml ? props.ml + "rem" : "0")};
  margin-right: ${props => (props.mr ? props.mr + "rem" : "0")};
  padding-top: ${props => (props.pt ? props.pt + "rem" : "0")};
  padding-bottom: ${props => (props.pb ? props.pb + "rem" : "0")};
  padding-left: ${props => (props.pl ? props.pl + "rem" : "0")};
  padding-right: ${props => (props.pr ? props.pr + "rem" : "0")};
`;

const StyledButton = props => {
  const {
    children,
    color = "primary",
    variant = "contained",
    fullWidth = false,
    size = "medium",
    callBack,
    disabled = false,
    ...rest
  } = props;

  return (
    <Btn
      variant={variant}
      color={color}
      fullWidth={fullWidth}
      size={size}
      onClick={callBack}
      disabled={disabled}
      {...rest}
    >
      {children}
    </Btn>
  );
};

StyledButton.propTypes = {
  callBack: PropTypes.func,
  classes: PropTypes.object,
  color: PropTypes.string,
  content: PropTypes.node,
  disabled: PropTypes.bool,
  fullWidth: PropTypes.bool,
  size: PropTypes.string,
  variant: PropTypes.string
};

export default StyledButton;
