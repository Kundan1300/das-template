import React from "react";
import TextField from "@material-ui/core/TextField";
import styled from "styled-components";

const InputField = styled(TextField)`
  margin-top: ${props => (props.mt ? props.mt + "rem" : "0")};
  margin-bottom: ${props => (props.mb ? props.mb + "rem" : "0")};
  margin-left: ${props => (props.ml ? props.ml + "rem" : "0")};
  margin-right: ${props => (props.mr ? props.mr + "rem" : "0")};
  padding-top: ${props => (props.pt ? props.pt + "rem" : "0")};
  padding-bottom: ${props => (props.pb ? props.pb + "rem" : "0")};
  padding-left: ${props => (props.pl ? props.pl + "rem" : "0")};
  padding-right: ${props => (props.pr ? props.pr + "rem" : "0")};
`;

const StyledInput = props => {
  const {
    error = false,
    id,
    label = "",
    placeholder = "",
    defaultValue = "",
    value = "",
    errorText = "",
    variant = "outlined",
    ...rest
  } = props;

  return (
    <InputField
      error={error}
      id={id}
      label={label}
      placeholder={placeholder}
      value={value}
      defaultValue={defaultValue}
      helperText={errorText}
      variant={variant}
      {...rest}
    />
  );
};

export default StyledInput;
