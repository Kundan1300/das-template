import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import styled from "styled-components";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

const useStyles = makeStyles(theme => ({ checked: {} }));

const StyledCheckbox = styled(Checkbox)``;

export const WithoutLabelCheckbox = ({ ...props }) => {
  return <StyledCheckbox color="primary" {...props} />;
};

export const WithLabelCheckbox = ({
  color,
  label,
  labelPlacement,
  ...rest
}) => (
  <FormControlLabel
    control={<Checkbox color={"primary"} {...rest} />}
    label={label}
    labelPlacement={labelPlacement}
  />
);
