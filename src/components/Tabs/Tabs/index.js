import React, { Fragment } from "react";

import { StyledArrowTab, BasicTab } from "./StyledArrowTabs";
import Flex from "../../Flex";

const Tabs = ({ tabs, value, onChange, type }) => {
  return (
    <Fragment>
      <Flex row>
        {type === "arrow" &&
          tabs.map((tab, index) => (
            <StyledArrowTab
              key={tab}
              isSelected={index == value}
              onClick={() => onChange(index)}
            >
              <div>{tab}</div>
            </StyledArrowTab>
          ))}
        {type === "basic" &&
          tabs.map((tab, index) => (
            <BasicTab
              key={tab}
              isSelected={index == value}
              onClick={() => onChange(index)}
            >
              <div>{tab}</div>
            </BasicTab>
          ))}
      </Flex>
    </Fragment>
  );
};

export default Tabs;
