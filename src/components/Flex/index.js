import styled from "styled-components";

const Flex = styled.div`
  display: flex;
  flex-direction: ${props => (props.row ? props.row : "column")};
  margin-top: ${props => (props.mt ? props.mt + "rem" : "0")};
  margin-bottom: ${props => (props.mb ? props.mb + "rem" : "0")};
  margin-left: ${props => (props.ml ? props.ml + "rem" : "0")};
  margin-right: ${props => (props.mr ? props.mr + "rem" : "0")};
  padding-top: ${props => (props.pt ? props.pt + "rem" : "0")};
  padding-bottom: ${props => (props.pb ? props.pb + "rem" : "0")};
  padding-left: ${props => (props.pl ? props.pl + "rem" : "0")};
  padding-right: ${props => (props.pr ? props.pr + "rem" : "0")};
`;

export default Flex;
