import React, { Fragment } from "react";
import styled from "styled-components";
import { Switch, Route, withRouter } from "react-router-dom";

import Dashboard from "../Dashboard";
import Inbox from "../Inbox";
import Settings from "../Settings";
import Complaints from "../Complaints";
import Reports from "../Reports";
import Flex from "../../components/Flex";

const Wrap = styled(Flex)`
  width: 100%;
  height: 100vh;
  overflow-y: auto;
`;

const Content = props => {
  const { match, ...rest } = props;

  return (
    <Wrap {...rest}>
      <Switch>
        <Route exact path={`${match.url}`} component={Dashboard} />
        <Route exact path={`${match.url}dashboard`} component={Dashboard} />
        <Route exact path={`${match.url}inbox`} component={Inbox} />
        <Route exact path={`${match.url}settings`} component={Settings} />
        <Route exact path={`${match.url}complaints`} component={Complaints} />
        <Route path={`${match.url}reports`} component={Reports} />
      </Switch>
    </Wrap>
  );
};

export default withRouter(Content);
