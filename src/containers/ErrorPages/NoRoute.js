import React from "react";

import Flex from "../../components/Flex";

const NoRoute = () => <Flex>No route found</Flex>;

export default NoRoute;
