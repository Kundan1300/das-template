import styled from "styled-components";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItem from "@material-ui/core/ListItem";

import StyledDivider from "../../components/Divider";

export const StyledListItemIcon = styled(ListItemIcon)`
  min-width: 42px;
  color: rgba(255, 255, 255, 0.87);
`;

export const Divider = styled(StyledDivider)`
  background-color: ${props =>
    props.bcolor ? props.bcolor : "rgba(255, 255, 255, 0.35)"};
  height: 0.75px;
`;

export const StyledListItem = styled(ListItem)`
  border-bottom: 1px solid rgba(255, 255, 255, 0.35);
`;
