import React, { Fragment, useState } from "react";
import { Switch, Route } from "react-router-dom";

import Tabs from "../../components/Tabs/Tabs/index";

const tabs = ["Frontend", "Backend", "FullStack", "DataScientist"];

const Frontend = () => <div>Frontend</div>;
const Backend = () => <div>Backend</div>;
const FullStack = () => <div>FullStack</div>;
const DataScience = () => <div>DataScience</div>;
const getInitialTab = location => {
  const active = location.pathname.split("/")[2];
  if (active) {
    for (let i = 0; i < tabs.length; i++) {
      if (tabs[i].toLowerCase() === active) {
        return i;
      }
    }
  }
  return 0;
};

const Reports = ({ match, history, location }) => {
  const [tab, setTab] = useState(getInitialTab(location));

  const handleChange = newValue => {
    setTab(newValue);

    history.push(match.url + "/" + tabs[newValue].toLowerCase(), {
      from: location.pathname
    });
  };

  return (
    <Fragment>
      <Tabs type="arrow" value={tab} tabs={tabs} onChange={handleChange} />
      <Tabs type="basic" value={tab} tabs={tabs} onChange={handleChange} />
      <Switch>
        <Route exact path={`${match.url}/`} component={Frontend} />
        <Route path={`${match.url}/frontend`} component={Frontend} />
        <Route path={`${match.url}/backend`} component={Backend} />
        <Route path={`${match.url}/fullstack`} component={FullStack} />
        <Route path={`${match.url}/datascientist`} component={DataScience} />
      </Switch>
    </Fragment>
  );
};

export default Reports;
