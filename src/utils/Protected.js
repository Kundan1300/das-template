import React from "react";
import { Route, Redirect } from "react-router-dom";

const Protected = ({ route, ...props }) => {
  if (route === "dash" && !localStorage.getItem("token")) {
    return <Redirect to="/login" />;
  } else if (route === "login" && localStorage.getItem("token")) {
    const { from } = props.location.state || { from: { pathname: "/" } };

    return <Redirect to={from} />;
  }

  return <Route {...props} />;
};

export default Protected;
