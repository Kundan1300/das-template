import { createMuiTheme } from "@material-ui/core/styles";

export const lightTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#2f78c4"
    },
    secondary: {
      main: "#ff5200"
    },
    text: {
      primary: "#212121",
      secondary: "#000000de",
      disabled: "#a2a2a2",
      hint: "#909090"
    }
  }
});
